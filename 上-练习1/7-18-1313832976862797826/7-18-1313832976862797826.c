#include <stdio.h>
int main()
{
	int cnt[10]={0},o=0,w=0;
	char c;
	while((c=getchar())!=EOF)
		if(c>='0'&&c<='9')
			++cnt[c-'0'];
		else if(c=='\n'||c=='\t'||c==' ')
			++w;
		else
			++o;
	printf("digits =");
	for(int i=0;i<10;printf(" %d",cnt[i]),++i);
	printf(", white space = %d, other = %d",w,o);
 	return 0;
}