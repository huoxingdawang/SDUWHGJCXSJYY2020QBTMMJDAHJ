#include <stdio.h>

/* declare power function */
int power(int base, int n);

int main()
{

  int i;

  for (i = 0; i < 10; ++i)
      printf("%d %d %d\n", i, power(2, i), power(-3, i));

  return 0;
}

/* 
Your code will be copied to this place.
*/
int power(int base, int n)
{
	int ans=1;
	while(n--)ans*=base;
	return ans;
}