int atoi_naive( const char s[])
{
	int ans=0;
	for(int i=0;s[i];ans=(ans<<3)+(ans<<1)+s[i]-'0',++i);
	return ans;	
}