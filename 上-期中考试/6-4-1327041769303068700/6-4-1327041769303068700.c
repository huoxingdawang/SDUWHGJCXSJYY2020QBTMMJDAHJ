void reverse(char s[])
{
	int l=0;
	for(;s[l];++l);
	for(int i=0,n=(l>>1);i<n;++i)
	{
		char b=s[i];
		s[i]=s[l-i-1];
		s[l-i-1]=b;
	}
}