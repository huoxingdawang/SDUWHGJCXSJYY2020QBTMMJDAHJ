#include <stdio.h>
int digit[10],ws,o;
int main()
{
	for(char c;(c=getchar())!=EOF;)
		if(c>='0'&&c<='9')
			++digit[c-'0'];
		else if(c==' '||c=='\n'||c=='\t')
			++ws;
		else
			++o;
	printf("digits =");
	for(int i=0;i<10;printf(" %d",digit[i]),++i);
	printf(", white space = %d, other = %d\n",ws,o);
	return 0;
}
