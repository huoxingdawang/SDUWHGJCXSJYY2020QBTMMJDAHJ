#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}

int main()
{
    double x,ans=1;
    scanf("%lf",&x);
    double mi=1,jiecheng=1;
    for(int i=1;;++i)
    {
        jiecheng*=i;
        mi*=x;
        double tmp=mi/jiecheng;
        ans+=tmp;
        if(tmp<0.00001)
            break;
    }
    printf("%.4f",ans);
    
 	return 0;
}
