#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define MAXWORD 100
/* #define NKEYS (sizeof keytab / sizeof(struct key)) */
#define NKEYS (sizeof keytab / sizeof(keytab[0]))
int getch(void);
struct key {
    char *word;
    int count;
} keytab[] = {
    { "auto", 0 },
    { "break", 0 },
    { "case", 0 },
    { "char", 0 },
    { "const", 0 },
    { "continue", 0 },
    { "default", 0 },
    /* ... */
    { "unsigned", 0 },
    { "void", 0 },
    { "volatile", 0 },
    { "while", 0 }
};

int getword(char *, int);
int binsearch(char *, struct key *, int);

/* count C keywords */
int main()
{
    int n;
    char word[MAXWORD];

    while (getword(word, MAXWORD) != EOF)
        if (isalpha(word[0]))
            if ((n = binsearch(word, keytab, NKEYS)) >= 0)
                keytab[n].count++;

    for (n = 0; n < NKEYS; n++)
        if (keytab[n].count > 0)
            printf("%4d %s\n", keytab[n].count, keytab[n].word);

    return 0;
}



/* 请在这里填写答案 */
int getword(char *s, int n)
{
    int i;
    char c;
    while((s[0]=c=getch())==' '||c=='\t');
    s[1]=0;
    if(!isalpha(c))return c;
    i=0;
    while(i<n&&isalpha(s[++i]=c=getch()));
    s[i]=0;
    return s[0];
}
int binsearch(char *word, struct key *keytab, int n)
{
    int l=0,r=n-1;
    while(l<=r)
    {
        int m=(l+r)>>1;
        char res=strcmp(word,keytab[m].word);
        if(res==0)      return m;
        else if(res<0)  r=m-1;
        else            l=m+1;
    }
    return -1;
}




#define BUFSIZE 100

char buf[BUFSIZE]; /* buffer for ungetch */
int bufp = 0; /* next free position in buf */

/* get a (possibly pushed-back) character */
int getch(void) 
{
    return (bufp > 0) ? buf[--bufp] : getchar();
}

/* push character back on input */
void ungetch(int c) 
{
    if (bufp >= BUFSIZE)
        printf("ungetch: too many characters\n");
    else
        buf[bufp++] = c;
}
