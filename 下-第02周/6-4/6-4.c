#include <stdio.h>

int narcissistic( int number );
void PrintN( int m, int n );

int main()
{
    int m, n;

    scanf("%d %d", &m, &n);
    if ( narcissistic(m) ) printf("%d is a narcissistic number\n", m);
    PrintN(m, n);
    if ( narcissistic(n) ) printf("%d is a narcissistic number\n", n);

    return 0;
}

/* 你的代码将被嵌在这里 */
int pow(int n,int m)
{
	int ans=1;
	while(--m)
		ans*=n;
	return ans;
}
int narcissistic( int number )
{
	int l=0,a=number;
	int num[10]={0};
	int ans=0;
	while(number)
	{
		num[l]=number%10;
		number/=10;
		++l;
	}
	for(int i=0;i<l;++i)
		ans+=pow(num[i],l+1);
	return ans==a;
}
void PrintN( int m, int n )
{
	for(++m;m<n;++m)
		if(narcissistic(m))
			printf("%d\n",m);
	
}