#include <stdio.h>
#include <math.h>

double funcos( double e, double x );

int main()
{    
    double e, x;

    scanf("%lf %lf", &e, &x);
    printf("cos(%.2f) = %.6f\n", x, funcos(e, x));

    return 0;
}

/* 你的代码将被嵌在这里 */
double funcos( double e, double x )
{
    double ans=1,xx=x*x,ji=2;
    for(int i=3,fuhao=-1;;i+=2)
    {
        double tmp=xx/ji;
        ans+=tmp*fuhao;
        if(fabs(tmp)<e)break;
        fuhao*=-1;
        xx*=x*x;
        ji*=i*(i+1);
    }
    
    return ans;
}