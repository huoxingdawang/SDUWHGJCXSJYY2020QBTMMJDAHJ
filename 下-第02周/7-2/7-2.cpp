#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <cstdlib>
#include <map>
#include <ctime>
#include <vector>
#include <queue>
#define debuglog(x) cerr<<"\tdebug:"<<#x<<endl
#define debug(x) cerr<<"\tdebug:"<<#x<<"="<<(x)<<endl
#define debugg(x,y) cerr<<"\tdebug:"<<(x)<<":"<<#y<<"="<<(y)<<endl
#define debugzu(x,a,b) 	cerr<<"\tdebug:"<<#x<<"=\n\t";for(int i=a;i<b;i++)cerr<<x[i]<<" ";fprintf(stderr,"\n");
#define debugerzu(x,a,b,c,d) 	cerr<<"\tdebug:"<<#x<<"=\n\t";for(int i=a;i<b;i++,fprintf(stderr,"\n\t"))for(int j=c;j<d;j++)cerr<<x[i][j]<<" ";fprintf(stderr,"\n");
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned
#define START clock_t __start=clock();
#define STOP fprintf(stderr,"\n\nUse Time:%fs\n",((double)(clock()-__start)/CLOCKS_PER_SEC));
using namespace std;
I LL read()
{
	R LL x;R bool f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}
#define MAXN 10
int data[MAXN][MAXN];
bool hang_max[MAXN][MAXN];
bool lie_min [MAXN][MAXN];




int main()
{
 	freopen("7-2.in","r",stdin);
// 	freopen("7-2.out","w",stdout);
    int n=read();
    for(int i=0;i<n;++i)
        for(int j=0;j<n;++j)
            data[i][j]=read();
//处理行最大
    for(int i=0;i<n;++i)
    {
        int maxx=data[i][0];
        for(int j=1;j<n;++j)
            maxx=max(maxx,data[i][j]);
        for(int j=0;j<n;++j)
            if(maxx==data[i][j])
                hang_max[i][j]=true;
    } 
//处理列最小
    for(int j=0;j<n;++j)
    {
        int minn=data[0][j];
        for(int i=1;i<n;++i)
            minn=min(minn,data[i][j]);
        for(int i=0;i<n;++i)
        if(minn==data[i][j])
            lie_min[i][j]=true;
    }
    bool flag=0;
    for(int i=0;i<n;++i)
        for(int j=0;j<n;++j)
            if(hang_max[i][j]&&lie_min[i][j])
                printf("%d %d\n",i,j),flag=1;
    if(!flag)
        printf("NONE");
    
	fclose(stdin);
	fclose(stdout);
 	return 0;
}