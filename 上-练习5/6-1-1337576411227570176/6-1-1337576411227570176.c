#include<stdio.h>

void swap(int *px, int *py);

int main()
{
    int a;
    int b;

    scanf("%d%d", &a, &b);

    printf("a = %d, b = %d\n", a, b);

    swap(&a, &b);

    printf("a = %d, b = %d\n", a, b);

    return 0;
}


/* 请在这里填写答案 */
void swap(int *px, int *py)
{
    int b=*px;
    *px=*py;
    *py=b;
}

