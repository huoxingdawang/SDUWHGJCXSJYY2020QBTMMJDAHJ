#include<stdio.h>
#include <ctype.h>

#define SIZE 100

int getch(void); // defined already
void ungetch(int); // deined already
int getint(int *pn);

int main()
{
    int n, array[SIZE];

    for (n = 0; n < SIZE && getint(&array[n]) != EOF && array[n] != 0; n++)
        printf("%d\n", array[n]);

    return 0;
}

/* 请在这里填写答案 */
int getint(int *pn)
{
	int x;char f;char c;
	for (f=0; (c=getch())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getch())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
    ungetch(c);
	*pn=f?-x:x;
    return *pn;
}
