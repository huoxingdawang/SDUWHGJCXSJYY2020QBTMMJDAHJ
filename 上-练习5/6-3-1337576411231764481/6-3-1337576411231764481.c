#include <stdio.h>

unsigned strlen_my(char *s);

int main()
{
    char array[100] = "hello, world";
    char *ptr = array;

    printf("%d\n", strlen_my("hello, world")); /* string constant */
    printf("%d\n", strlen_my(array));          /* char array[100]; */
    printf("%d\n", strlen_my(ptr));            /* char *ptr; */

    while(~scanf("%s", array)) {
        printf("%d\n", strlen_my(array));
    }

    return 0;
}


/* 请在这里填写答案 */
unsigned strlen_my(char *s)
{
    unsigned x=0;
    while(s[x])++x;
    return x;
}