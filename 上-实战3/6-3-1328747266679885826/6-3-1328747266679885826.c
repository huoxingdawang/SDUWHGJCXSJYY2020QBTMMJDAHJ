#include <stdio.h>
#include <assert.h>
const int N = 10001;

void expand(char [], char []);

int main()
{
//    freopen("0.in", "r", stdin);
//    freopen("0.out", "w", stdout);

    char s1[N], s2[N * 30];

    while(scanf("%s", s1) != EOF) {
        expand(s1, s2);
        printf("%s\n", s2);
    }

    return 0;
}

/* 请在这里填写答案 */
char check(char c)
{
	if(c>='0'&&c<='9')           return 0;
	else if(c >= 'a' && c <= 'z')return 1;
	else if(c >= 'A' && c <= 'Z')return 2;
	return -1;
}
void expand(char s1[],char s2[])
{
	int i=0,k=0;
//	if(s1[i])s2[k++]=s1[i++];
	while(s1[i])
	{
		if(s1[i]=='-'&&s1[i+1])
		{
			char c1=check(s1[i-1]);
			char c2=check(s1[i+1]);
			if(c1==c2&&c1!=-1&&s1[i-1]<s1[i+1])
			{
				for(char j=s1[i-1]+1;j<s1[i+1];s2[k]=j,++k,++j);
				s2[k]=s1[i+1];
                ++k;
				i+=2;
			}
			else if(c1==c2&&c1!=-1&&s1[i-1]==s1[i+1])
				i+=2;
			else
				s2[k]=s1[i],++k,++i;
		}
		else
            s2[k]=s1[i],++k,++i;
	}
	s2[k]=0;
}