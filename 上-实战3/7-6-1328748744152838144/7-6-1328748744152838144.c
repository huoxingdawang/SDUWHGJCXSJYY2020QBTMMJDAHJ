#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}
#define MAXM 111



int fen[MAXM];
char que_ans[MAXM];
int main()
{
    int n=read(),m=read();
    for(int i=0;i<m;fen[i]=read(),++i);
    for(int i=0;i<m;que_ans[i]=read(),++i);
    for(int j=0;j<n;++j)
    {
        int ans=0;
        for(int i=0;i<m;ans+=(que_ans[i]==read()?fen[i]:0),++i);
        printf("%d\n",ans);
    }
 	return 0;
}
