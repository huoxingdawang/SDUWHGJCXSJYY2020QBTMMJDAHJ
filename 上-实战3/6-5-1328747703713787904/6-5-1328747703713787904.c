#include <stdio.h>

int mygcd( int x, int y );

int main()
{
    int x, y;

    scanf("%d %d", &x, &y);
    printf("%d\n", mygcd(x, y));

    return 0;
}

/* Your code is placed here */
int mygcd( int x, int y ){return y?mygcd(y,x%y):x;}
