#include <stdio.h>
int main()
{
    int n;
    scanf("%d",&n);
    if(n<100||n>999)
        printf("Invalid Value.\n");
    else
    {
        int a=n%10;
        int b=n/10%10;
        int c=n/100%10;
        if(a*a*a+b*b*b+c*c*c==n)
            printf("Yes\n");
        else
            printf("No\n");
    }
}