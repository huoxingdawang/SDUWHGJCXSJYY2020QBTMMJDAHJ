#include <stdio.h>
#include <math.h>

#define Cube(x) ...(略)...

void Folium(double *x, double *y, double a, double t);

int main()
{
    double a, t, x, y;
    scanf("%lg%lg", &a, &t);
    Folium(&x, &y, a, t);
    printf("%f %f\n", x, y);
    return 0;
}

/* 你提交的代码将被嵌在这里 */
void Folium(double *x, double *y, double a, double t)
{
	*x=3*a*t/(1+Cube(t));
	*y=3*a*t*t/(1+Cube(t));
}