#include <cstdio>
#include <iostream>
#include <algorithm>
using namespace std;
#define L long
#define LL L L
#define I inline
#define R register
I LL read()
{
	R char c;R bool f;R LL x;
	for(f=0;(c=getchar())<'0'||c>'9';f=(c=='-'));
	for(x=c-'0';(c=getchar())>='0'&&c<='9';x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}

#define MAXN 1011
int teams[MAXN];
int main()
{
//	freopen("7-1.in","r",stdin);
//	freopen("7-1.out","w",stdout);
	int n=read();
	for(int i=0;i<n;++i)
	{
		int t,n,s;
		scanf("%d-%d %d",&t,&n,&s);
		teams[t]+=s;
	}
	int ans=0;
	for(int i=0;i<MAXN;++i)
		if(teams[i]>teams[ans])
			ans=i;
	printf("%d %d",ans,teams[ans]);
	return 0;
}
