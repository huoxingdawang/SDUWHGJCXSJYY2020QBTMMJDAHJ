#include <stdio.h>
#define MAXS 20

void zip( char *p );
void ReadString( char *s ); /* 由裁判实现，略去不表 */

int main()
{
    char s[MAXS];

    ReadString(s);
    zip(s);
    printf("%s\n", s);

    return 0;
}

/* 请在这里填写答案 */
void zip( char *p )
{
	int j=0;
	char last=p[0];
	for(int i=0,cnt=0;;++i)
	{
		if(last!=p[i])
		{
			if(cnt>1)
				j+=sprintf(p+j,"%d",cnt);
			p[j]=last;
			++j;
			last=p[i];
			cnt=1;
		}
		else
			++cnt;
		if(!p[i])
			break;	
	}
	p[j]=0;
}