#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}
#define MAXN 22
#define MAXM 22
int a[MAXN][MAXM];
char gan(int i,int j){return a[i][j]>a[i-1][j]&&a[i][j]>a[i+1][j]&&a[i][j]>a[i][j-1]&&a[i][j]>a[i][j+1];}


int main()
{
    int n=read(),m=read();
    for(int i=0;i<n;++i)
        for(int j=0;j<m;++j)
            a[i][j]=read();
    char flag=0;
    for(int i=1;i<n-1;++i)
        for(int j=1;j<m-1;++j)
            if(gan(i,j))
            {
                flag=1;
                printf("%d %d %d\n",a[i][j],i+1,j+1);
            }
    if(!flag)
        printf("None %d %d",n,m);
    
    
 	return 0;
}
