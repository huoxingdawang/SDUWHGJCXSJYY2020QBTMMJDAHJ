#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}


#define MAXN 11
int data[MAXN][MAXN];

int main()
{
    int t=read();
    while(t--)
    {
        int n=read();
        for(int i=0;i<n;++i)
            for(int j=0;j<n;++j)
                data[i][j]=read();
        for(int i=0;i<n;++i)
            for(int j=0;j<i;++j)
                if(data[i][j])
                {
                    printf("NO\n");
                    goto notok;
                }
        printf("YES\n");
notok:;
    }
 	return 0;
}
