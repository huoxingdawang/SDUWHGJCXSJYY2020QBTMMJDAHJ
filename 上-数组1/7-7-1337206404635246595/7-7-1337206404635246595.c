#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}
int main()
{
    int n=read(),m=read();
    for(int i=0,ans=0;i<n;++i,printf("%d\n",ans),ans=0)
        for(int j=0;j<m;++j)
            ans+=read();
 	return 0;
}
