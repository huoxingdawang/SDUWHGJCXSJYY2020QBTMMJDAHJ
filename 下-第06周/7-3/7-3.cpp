#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <cstdlib>
#include <map>
#include <ctime>
#include <vector>
#include <queue>
#define debuglog(x) cerr<<"\tdebug:"<<#x<<endl
#define debug(x) cerr<<"\tdebug:"<<#x<<"="<<(x)<<endl
#define debugg(x,y) cerr<<"\tdebug:"<<(x)<<":"<<#y<<"="<<(y)<<endl
#define debugzu(x,a,b) 	cerr<<"\tdebug:"<<#x<<"=\n\t";for(int i=a;i<b;i++)cerr<<x[i]<<" ";fprintf(stderr,"\n");
#define debugerzu(x,a,b,c,d) 	cerr<<"\tdebug:"<<#x<<"=\n\t";for(int i=a;i<b;i++,fprintf(stderr,"\n\t"))for(int j=c;j<d;j++)cerr<<x[i][j]<<" ";fprintf(stderr,"\n");
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned
#define START clock_t __start=clock();
#define STOP fprintf(stderr,"\n\nUse Time:%fs\n",((double)(clock()-__start)/CLOCKS_PER_SEC));
using namespace std;
I LL read()
{
	R LL x;R bool f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}
char data[100][100];
int mapp[100];
int need(char a[],char b[])
{
    int al=strlen(a);
    int bl=strlen(b);
    return al>bl;
    for(int i=0;i<al;++i)
        if(a[i]!=b[i])
            return a[i]>b[i];
}




int main()
{
 	freopen("7-3.in","r",stdin);
// 	freopen("7-3.out","w",stdout);
    int l=0;
    for(;;)
    {
        scanf("%s",data[l]);
        if(data[l][0]=='#')
            break;
        mapp[l]=l;
        ++l;
    }
    for(int i=0;i<l;++i)
        for(int j=i+1;j<l;++j)
            if(need(data[mapp[i]],data[mapp[j]]))
            {
                int c=mapp[i];
                mapp[i]=mapp[j];
                mapp[j]=c;
            }
    for(int i=0;i<l;++i)
        printf("%s ",data[mapp[i]]);

	fclose(stdin);
	fclose(stdout);
 	return 0;
}