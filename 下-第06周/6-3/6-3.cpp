#include <stdio.h>
#define MAXS 20

void f( char *p );
void ReadString( char *s ); /* 由裁判实现，略去不表 */

int main()
{
    char s[MAXS]="Hello World!";

//  ReadString(s);
    f(s);
    printf("%s\n", s);

    return 0;
}

/* 你的代码将被嵌在这里 */
void f( char *p )
{
    int l=0;
    while(p[l])++l;
    for(int i=0;i<l/2;++i)
    {
        char c=p[i];
        p[i]=p[l-1-i];
        p[l-1-i]=c;
    }
}