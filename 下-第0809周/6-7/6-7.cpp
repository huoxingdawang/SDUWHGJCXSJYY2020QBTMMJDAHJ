#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct stud_node {
     int    num;
     char   name[20];
     int    score;
     struct stud_node *next;
};
struct stud_node *head, *tail;

void input();

int main()
{
    struct stud_node *p;

    head = tail = NULL;
    input();
    for ( p = head; p != NULL; p = p->next )
        printf("%d %s %d\n", p->num, p->name, p->score);

    return 0;
}

/* 你的代码将被嵌在这里 */
struct stud_node *createlist()
{
    struct stud_node *head=NULL;
    struct stud_node *tail=NULL;
    while(1)
    {
        struct stud_node *tmp=(struct stud_node *)malloc(sizeof(struct stud_node));
        scanf("%d",&tmp->num);
        if(tmp->num==0)
            break;
        scanf("%s%d",tmp->name,&tmp->score);
        tmp->next=NULL;
        if(tail)
            tail->next=tmp,tail=tmp;
        else 
            head=tail=tmp;
    }
    return head;
}
void input()
{
    head=createlist();
}