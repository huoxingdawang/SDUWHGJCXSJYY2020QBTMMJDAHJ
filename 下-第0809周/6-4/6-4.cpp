#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int data;
    struct ListNode *next;
};

struct ListNode *createlist();
struct ListNode *deleteeven( struct ListNode *head );
void printlist( struct ListNode *head )
{
     struct ListNode *p = head;
     while (p) {
           printf("%d ", p->data);
           p = p->next;
     }
     printf("\n");
}

int main()
{
    struct ListNode *head;

    head = createlist();
    printlist(head);
    head = deleteeven(head);
    printlist(head);

    return 0;
}

/* 你的代码将被嵌在这里 */
struct ListNode *createlist()
{
    struct ListNode *head=NULL;
    struct ListNode *tail=NULL;
    while(1)
    {
        struct ListNode *tmp=(struct ListNode *)malloc(sizeof(struct ListNode));
        scanf("%d",&tmp->data);
        if(tmp->data==-1)
            break;
        tmp->next=NULL;
        if(tail)
            tail->next=tmp,tail=tmp;
        else 
            head=tail=tmp;
    }
    return head;
}
struct ListNode *deleteeven( struct ListNode *head )
{
    struct ListNode * now=head,*pre=NULL,*tmp=NULL;
    for(;now;)
        if((now->data&1)==0)
        {
            if(pre)
                tmp=pre->next=now->next;
            else
                tmp=head=now->next;
            free(now);
            now=tmp;
        }
        else
            pre=now,now=now->next;
    return head;
}
/*比较鸡贼的方法，直插入基数
struct ListNode *createlist()
{
    struct ListNode *head=NULL;
    struct ListNode *tail=NULL;
    while(1)
    {
        struct ListNode *tmp=(struct ListNode *)malloc(sizeof(struct ListNode));
        scanf("%d",&tmp->data);
        if(tmp->data==-1)
            break;
        if(tmp->data&1)
        {
            tmp->next=NULL;
            if(tail)
                tail->next=tmp,tail=tmp;
            else 
                head=tail=tmp;
        }
        else
            free(tmp);
    }
    return head;
}
struct ListNode *deleteeven( struct ListNode *head )
{
    return head;
}
*/