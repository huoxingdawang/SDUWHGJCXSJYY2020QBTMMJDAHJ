#include <stdio.h>
#include <stdlib.h>

typedef int ElementType;
typedef struct Node *PtrToNode;
struct Node {
    ElementType Data;
    PtrToNode   Next;
};
typedef PtrToNode List;

List Read(); /* 细节在此不表 */
void Print( List L ); /* 细节在此不表 */

List Insert( List L, ElementType X );

int main()
{
    List L;
    ElementType X;
    L = Read();
    scanf("%d", &X);
    L = Insert(L, X);
    Print(L);
    return 0;
}

/* 你的代码将被嵌在这里 */
List Insert( List L, ElementType X )
{
    struct Node * now=L->Next,*pre=NULL;
    for(;now&&now->Data<=X;pre=now,now=now->Next);
    struct Node *tmp=malloc(sizeof(struct Node));
    tmp->Data=X;
    tmp->Next=now;
    if(pre)
        pre->Next=tmp;
    else
        L->Next=tmp;
    return L;
}