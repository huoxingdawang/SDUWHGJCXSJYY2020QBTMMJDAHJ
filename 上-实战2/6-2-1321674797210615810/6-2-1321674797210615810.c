#include <stdio.h>

void squeeze(char[], char[]);

int main()
{
    char s1[1001];
    char s2[1001];

    scanf("%s%s", s1, s2);
    squeeze(s1, s2);

    printf("%s\n", s1);

    return 0;
}

/* Your code will be copied here. */
void squeeze(char s1[], char s2[])
{
    char s[256];
    for(int i=0;i<256;s[i]=0,++i);
    for(int i=0;s2[i];s[(int)s2[i]]=1,++i);
	int j=0;
	for(int i=0;s1[i];++i)
		if(!s[(int)s1[i]])
			s1[j++]=s1[i];
	s1[j]=0;
}