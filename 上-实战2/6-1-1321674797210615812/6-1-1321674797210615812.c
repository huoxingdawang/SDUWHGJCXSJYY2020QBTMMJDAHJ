#include <stdio.h>
#define YES 1
#define NO 0
/* htoi:convert hexadecimal string s to integer */
int htoi(char s[]);

int main()
{
//    freopen("2.in", "r", stdin);
//    freopen("2.out", "w", stdout);
    char A[12];

    while(scanf("%s", A) != EOF) {
        int v = htoi(A);
        printf("%d\n", v);
    }

    return 0;
}

/* Your code will be copied here.*/
int htoi(char s[])
{
    int n=0;
    for(int i=0;s[i];++i)
    {
        if(s[i]>='0'&&s[i]<='9')
            n=(n<<4)+s[i]-'0';
        else if(s[i]>='a'&&s[i]<='f')
            n=(n<<4)+s[i]-'a'+10;
        else if(s[i]>='A'&&s[i]<='F')
            n=(n<<4)+s[i]-'A'+10;
    }
    return n;
}