
#include <stdio.h>
#include <stdlib.h>

typedef int ElemType;
typedef struct LNode
{
    ElemType data;
    struct LNode *next;
}LNode,*LinkList;

LinkList Create();/* 细节在此不表 */

int Locate ( LinkList L, ElemType e);

int main()
{
    ElemType e;
    LinkList L = Create();
    scanf("%d",&e);
    printf("%d\n", Locate(L,e));
    return 0;
}

/* 你的代码将被嵌在这里 */
int Locate ( LinkList L, ElemType e)
{
    int ans=1;
    for(L=L->next;L;L=L->next)
    {
        if(L->data==e)
            return ans;
        ++ans;
    }
    return 0;
}