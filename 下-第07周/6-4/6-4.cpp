#include <stdio.h>
#include <stdlib.h>

struct ListNode {
    int data;
    struct ListNode *next;
};

struct ListNode *createlist();

int main()
{
    struct ListNode *p, *head = NULL;

    head = createlist();
    for ( p = head; p != NULL; p = p->next )
        printf("%d ", p->data);
    printf("\n");

    return 0;
}

/* 你的代码将被嵌在这里 */
struct ListNode *createlist()
{
    struct ListNode * head;
    for(;;)
    {
        int a;
        scanf("%d",&a);
        if(a==-1)break;
        struct ListNode * tmp=malloc(sizeof(struct ListNode));
        tmp->next=head;
        tmp->data=a;
        head=tmp;
    }
    return head;
}