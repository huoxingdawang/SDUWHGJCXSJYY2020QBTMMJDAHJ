#include <stdio.h>
#include <stdlib.h>

void WriteArticle(FILE *f);

int main()
{
    FILE *f;
    f = fopen("Article.txt", "w");
    if (!f)
    {
        puts("文件无法打开!");
        exit(1);
    }

    WriteArticle(f);

    if (fclose(f))
    {
        puts("文件无法关闭!");
        exit(1);
    }
    puts("文件保存成功!");
    return 0;
}

/* 你提交的代码将被嵌在这里 */
void WriteArticle(FILE *f)
{
    for(char c;(c=getchar())!=EOF;putc(c,f));
}
