#include <stdio.h>
#include <math.h>
float fun(float eps);

int main()
{
float  eps;
scanf("%f", &eps);
 printf("%.4f\n",fun(eps));
return 0;
}


/* 请在这里填写答案 */
float fun(float eps)
{
    float ans=0;
    for(int i=1,f=1;;i+=2,f*=-1)
    {
        float tmp=((float)f)/i;
        if(fabs(tmp)<eps)
            break;
        ans+=tmp;
    }
    return ans*4;
}