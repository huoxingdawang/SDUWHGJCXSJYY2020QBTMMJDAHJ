#include<stdio.h>
#include <ctype.h>

double atof(char s[]);

int main()
{
    char s[100];
    while(scanf("%s", s) != EOF) {
        printf("%f\n", atof(s));
    }

    return 0;
}

/* 请在这里填写答案 */
double atof(char s[])
{
    double a;
    sscanf(s,"%lf",&a);
    return a;
}