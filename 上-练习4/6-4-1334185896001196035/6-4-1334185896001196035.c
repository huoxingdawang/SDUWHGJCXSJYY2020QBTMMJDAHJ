#include <stdio.h>
#include <stdlib.h> /* for atof() */

#define MAXOP 100 /* max size of operand or operator */
#define NUMBER '0' /* signal that a number was found */

int getop(char []);
void push(double);
double pop(void);

/* reverse Polish calculator */
int main()
{
    int type;
    double op2;
    char s[MAXOP];

    while ((type = getop(s)) != EOF) {
        switch (type) {
            case NUMBER:
                push(atof(s));
                break;
            case '+':
                push(pop() + pop());
                break;
            case '*':
                push(pop() * pop());
                break;
            case '-':
                op2 = pop();
                push(pop() - op2);
                break;
            case '/':
                op2 = pop();
                if (op2 != 0.0)
                    push(pop() / op2);
                else
                    printf("error: zero divisor\n");
                break;
            case '\n':
                printf("%.8g\n", pop());
                break;
            default:
                printf("error: unknown command %s\n", s);
                break;
        }
    }

    return 0;
}

#include <ctype.h>

int getch(void);// already defined, get a character
void ungetch(int);// already defined, pop back a character

#define MAXVAL 100 /* maximum depth of val stack */

int sp = 0; /* next free stack position */
double val[MAXVAL]; /* value stack */

int getop(char s[])
{
    int i;
    char c;
    while((s[0]=c=getch())==' '||c=='\t');
    s[1]=0;
    if((c<'0'||c>'9')&&c!='.')return c;
    i=0;
    if((c>='0'&&c<='9'))while((s[++i]=c=getch())>='0'&&c<='9');
    if(c=='.')while((s[++i]=c=getch())>='0'&&c<='9');
    s[i]=0;
    if(c!=EOF)ungetch(c);
    return NUMBER;
}
void push(double a)
{
    if(sp<MAXVAL)
        val[sp++]=a;
    else
        printf("error: stack full, can't push %g\n",a);
}
double pop(void)
{
    if(sp>0)
        return val[--sp];
    printf("error: stack empty\n");
    return 0;
}



#define BUFSIZE 100

char buf[BUFSIZE]; /* buffer for ungetch */
int bufp = 0; /* next free position in buf */

/* get a (possibly pushed-back) character */
int getch(void)
{
    return (bufp > 0) ? buf[--bufp] : getchar();
}

/* push character back on input */
void ungetch(int c)
{
    if (bufp >= BUFSIZE)
        printf("ungetch: too many characters\n");
    else
        buf[bufp++] = c;
}