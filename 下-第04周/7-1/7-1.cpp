#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <cstdlib>
#include <map>
#include <ctime>
#include <vector>
#include <queue>
#define debuglog(x) cerr<<"\tdebug:"<<#x<<endl
#define debug(x) cerr<<"\tdebug:"<<#x<<"="<<(x)<<endl
#define debugg(x,y) cerr<<"\tdebug:"<<(x)<<":"<<#y<<"="<<(y)<<endl
#define debugzu(x,a,b) 	cerr<<"\tdebug:"<<#x<<"=\n\t";for(int i=a;i<b;i++)cerr<<x[i]<<" ";fprintf(stderr,"\n");
#define debugerzu(x,a,b,c,d) 	cerr<<"\tdebug:"<<#x<<"=\n\t";for(int i=a;i<b;i++,fprintf(stderr,"\n\t"))for(int j=c;j<d;j++)cerr<<x[i][j]<<" ";fprintf(stderr,"\n");
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned
#define START clock_t __start=clock();
#define STOP fprintf(stderr,"\n\nUse Time:%fs\n",((double)(clock()-__start)/CLOCKS_PER_SEC));
using namespace std;
I LL read()
{
	R LL x;R bool f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}
#define MAXN 100
int ans[MAXN][MAXN];
void gan(int cnt,int s,int n)
{
    if(n<=0)
        return;
    for(int i=s;i<s+n;++i)
        ans[i][s]=cnt,++cnt;
    for(int i=s+1;i<s+n;++i)
        ans[s+n-1][i]=cnt,++cnt;
    for(int i=s+n-2;i>s;--i)
        ans[i][s+n-1]=cnt,++cnt;
    for(int i=s+n-1;i>s;--i)
        ans[s][i]=cnt,++cnt;
    gan(cnt,s+1,n-2);
}


int main()
{
 	freopen("7-1.in","r",stdin);
// 	freopen("7-1.out","w",stdout);
    int n=read();
    gan(1,0,n);
    for(int j=0;j<n;++j,putchar('\n'))
        for(int i=0;i<n;++i)
            printf("%3d",ans[i][j]);

	fclose(stdin);
	fclose(stdout);
 	return 0;
}