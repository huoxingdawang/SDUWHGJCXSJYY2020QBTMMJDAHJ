#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <cstring>
#include <string>
#include <cctype>
#include <cstdlib>
#include <map>
#include <ctime>
#include <vector>
#include <queue>
#define debuglog(x) cerr<<"\tdebug:"<<#x<<endl
#define debug(x) cerr<<"\tdebug:"<<#x<<"="<<(x)<<endl
#define debugg(x,y) cerr<<"\tdebug:"<<(x)<<":"<<#y<<"="<<(y)<<endl
#define debugzu(x,a,b) 	cerr<<"\tdebug:"<<#x<<"=\n\t";for(int i=a;i<b;i++)cerr<<x[i]<<" ";fprintf(stderr,"\n");
#define debugerzu(x,a,b,c,d) 	cerr<<"\tdebug:"<<#x<<"=\n\t";for(int i=a;i<b;i++,fprintf(stderr,"\n\t"))for(int j=c;j<d;j++)cerr<<x[i][j]<<" ";fprintf(stderr,"\n");
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned
#define START clock_t __start=clock();
#define STOP fprintf(stderr,"\n\nUse Time:%fs\n",((double)(clock()-__start)/CLOCKS_PER_SEC));
using namespace std;
int gao(char c)
{
    if(c>='a'&&c<='f')return c-'a'+10;
    if(c>='A'&&c<='F')return c-'A'+10;
    if(c>='0'&&c<='9')return c-'0';
    return -1;
}

int main()
{
 	freopen("7-2.in","r",stdin);
// 	freopen("7-2.out","w",stdout);
	R LL x=0;R bool f;R char c;
	for(f=0;(c=getchar())!='#'&&gao(c)==-1;f|=c=='-');
    if(c!='#')
    {
        for(x=gao(c);(c=getchar())!='#';)
            if(gao(c)!=-1)
                x=(x<<4)+gao(c);
    }
    else
        f=0;
    cout<<(f?-x:x);        
	fclose(stdin);
	fclose(stdout);
 	return 0;
}