#include<stdio.h>
#include<assert.h>

void sumoffour(int a[], int n, int sum);

int main()
{
    int a[15];
    int T;
    int n;
    int sum;

    scanf("%d", &T);

    while(T--) {
        scanf("%d%d", &n, &sum);
        assert(n <= sizeof(a));

        for(int i = 0; i < n; i++) {
            scanf("%d", &a[i]);
        }

        sumoffour(a, n, sum);
    }

    return 0;
}

/* 请在这里填写答案 */
void sumoffour(int a[], int n, int sum)
{
    char flag=0;
    for(int i=0;i<n;++i)
        for(int j=i+1;j<n;++j)
            for(int k=j+1;k<n;++k)
                for(int l=k+1;l<n;++l)
                    if(a[i]+a[j]+a[k]+a[l]==sum)
                        printf("The sum of %d, %d, %d, and %d is %d.\n",a[i],a[j],a[k],a[l],sum),flag=1;
    if(!flag)printf("No answer\n");
}