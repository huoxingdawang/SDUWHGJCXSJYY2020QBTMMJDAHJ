#include<stdio.h>
#include<stdlib.h>

int binsearch(int x, int v[], int n);
int compare (const void * a, const void * b);

int main ()
{
    int n;    // size of array v
    int m;  // number of calling search 

    scanf("%d", &n);

    int * v = (int*)malloc(sizeof(int) * n);

    for(int i = 0; i < n; i++) {
        scanf("%d", &v[i]);
    }

    qsort(v, n, sizeof(int), compare); // to sort array v by calling standard library function

    scanf("%d", &m);

    while(m--) {
        int x;
        scanf("%d", &x);
        printf("%d\n", binsearch(x, v, n));
    }

    free(v);

    return 0;
}

int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}
int binsearch(int x, int v[], int n)
{
    for(int l=0,r=n-1,m=0;l<=r;)
        if(v[m=((l+r)>>1)]==x)
            return m;
        else if(v[m]>x)
            r=m-1;
        else 
            l=m+1;
    return -1;
}
/*
int binsearch(int x, int v[], int n)
{
    for(int i=0;i<n;++i)
        if(v[i]==x)
            return i;
    return -1;
}
*/