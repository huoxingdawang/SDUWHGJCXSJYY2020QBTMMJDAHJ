#include <stdio.h>

void printLeapYear(int year);

/* a year is leap year or not*/
int main()
{
    printLeapYear(1990);
    printLeapYear(2000);
    printLeapYear(2005);
    printLeapYear(2008);

    return 0;
}

/* 请在这里填写答案 */
void printLeapYear(int year)
{
	printf("%d is %sa leap year\n",year,((year%4)==0&&(year%100!=0)||((year%400)==0))?"":"not ");	
}