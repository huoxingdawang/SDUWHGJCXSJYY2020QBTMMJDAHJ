#include <stdio.h>

unsigned getbits(unsigned x, int p, int n);
void display(unsigned x);

int main()
{
    unsigned x;
    int p, n;

    while(scanf("%u%d%d", &x, &p, &n) != EOF) {
        display( getbits(x, p, n) );
    }

    return 0;
}

void display(unsigned x)
{
    for(int i = 31; i >= 0; i--){
        printf("%d",(x>>i)&1);
    }
    putchar('\n');
}

/* 请在这里填写答案 */
unsigned getbits(unsigned x, int p, int n)
{
    return (x>>(p+1-n))&(~((~0ll)<<n));
}
