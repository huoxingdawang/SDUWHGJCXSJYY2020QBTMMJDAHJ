#include <stdio.h>

void isleap(int year);

/* a year is leap year or not*/
int main()
{
    int year;

    for(year = 1990; year <= 2100; year++){
        isleap(year);
    }

    return 0;
}

/* 请在这里填写答案 */
void isleap(int year)
{
	printf("%d is %sa leap year\n",year,((year%4)==0&&(year%100!=0)||((year%400)==0))?"":"not ");	
}