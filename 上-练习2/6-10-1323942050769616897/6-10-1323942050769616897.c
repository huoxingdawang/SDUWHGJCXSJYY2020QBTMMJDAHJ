#include <stdio.h>

void niceprint(int n);

int main()
{
    int n;

    while(~scanf("%d", &n)) {
        niceprint(n);
    }

    return 0;
}

/* 请在这里填写答案 */
void niceprint(int n)
{
    printf("The array has %d element%s as follows.\n",n,n==1?"":"s");
    for(int i=1;i<=n;++i)
        printf("%6d%c",i,(i%10==0||i==n)?'\n':' ');
}