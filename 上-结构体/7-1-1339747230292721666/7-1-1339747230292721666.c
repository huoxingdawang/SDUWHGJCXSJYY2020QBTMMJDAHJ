#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}
struct stu
{
    int xh;
    char ming[100];
    int fen;
};
struct stu stus[1000];



int main()
{
    int n=read();
    double fen=0;
    for(int i=0;i<n;++i)stus[i].xh=read(),scanf("%s",stus[i].ming),fen+=(stus[i].fen=read());
    fen/=n;
    printf("%.2lf\n",fen);
    for(int i=0;i<n;++i)
        if(stus[i].fen<fen)
            printf("%s %05d\n",stus[i].ming,stus[i].xh);
 	return 0;
}
