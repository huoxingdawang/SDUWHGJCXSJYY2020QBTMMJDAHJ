#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}
struct ren
{
    char ming[100];
    int sr;
    char tel[100];
};
struct ren rens[1000];
int cmp( const void *a,const void *b)
{
    return ((struct ren*)a)->sr-((struct ren*)b)->sr;
}
int main()
{
    int n=read();
    double fen=0;
    for(int i=0;i<n;++i)scanf("%s%d%s",rens[i].ming,&rens[i].sr,rens[i].tel);
    qsort(rens,n,sizeof(struct ren),cmp);
    for(int i=0;i<n;++i)printf("%s %d %s\n",rens[i].ming,rens[i].sr,rens[i].tel);
 	return 0;
}
