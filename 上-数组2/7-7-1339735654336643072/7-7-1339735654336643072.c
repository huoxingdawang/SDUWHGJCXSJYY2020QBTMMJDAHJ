#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}

int quan[]={7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2};
char M[]={"10X98765432"};
char check(char s[])
{
    int ans=0;
    for(int i=0;i<17;++i)
        if(s[i]<'0'||s[i]>'9')
            return 0;
        else
            ans+=(s[i]-'0')*quan[i];
    return M[ans%11]==s[17];
}



int main()
{
    int n=read();
    char flag=1;
    while(n--)
    {
        char s[100];
        scanf("%s",s);
        if(!check(s))
            printf("%s\n",s),flag=0;
    }
    if(flag)
        printf("All passed");
    
 	return 0;
}
