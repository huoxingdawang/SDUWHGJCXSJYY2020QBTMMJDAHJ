#include <stdio.h>
#define MAXN 20

void delchar( char *str, char c );
void ReadString( char s[] ); /* 由裁判实现，略去不表 */

int main()
{
    char str[MAXN], c;

    scanf("%c\n", &c);
    ReadString(str);
    delchar(str, c);
    printf("%s\n", str);

    return 0;
}

/* 你的代码将被嵌在这里 */
void delchar( char *str, char c )
{
    int j=0;
    for(int i=0;str[i];++i)
        if(c!=str[i])
            str[j]=str[i],++j;
    str[j]=0;
}
void ReadString( char s[] )
{
    scanf("%s",s);
}
