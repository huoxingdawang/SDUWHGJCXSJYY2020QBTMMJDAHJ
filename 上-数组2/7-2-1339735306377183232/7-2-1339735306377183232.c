#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define R register
#define L long
#define LL long long
#define I inline
#define U unsigned

I LL read()
{
	R LL x;R char f;R char c;
	for (f=0; (c=getchar())<'0'||c>'9'; f=c=='-');
	for (x=c-'0'; (c=getchar())>='0'&&c<='9'; x=(x<<3)+(x<<1)+c-'0');
	return f?-x:x;
}



int a[100][100];
int main()
{
    int m=read(),n=read();
    m%=n;
    for(int i=0;i<n;++i)for(int j=0;j<n;++j)a[i][j]=read();
    for(int i=0;i<n;++i,putchar('\n'))
        for(int j=0;j<n;++j)
            printf("%d ",a[i][(n-m+j)%n]);
    
 	return 0;
}
